# Salt Style-guide

## Process and Workflow

* Use SaltStack defaults unless noted otherwise
  * Jinja templating
  * YAML renderer
* Author State files for reuse
  * Each SLS file should do one thing
  * Parameterize Runtime Configuration
  * Be self-contained
    * Use **include** and **requisite_in** for explicit dependencies
  * Run from anywhere:
    * Standalone
    * Top files
    * Orchestrate
    * Reactors
* Gitignore binary files
* Add comments to the top of non-sls Salt managed files
  * Example:  
  `## This file is managed by Salt -- DO NOT edit by hand ##`
* Directory Structure
  * Keep as shallow as logically makes sense
  * Non-sls files should go in the `files` directory in the corresponding formuala directory
    * ie. `salt/wildfly/files` or `salt/apache/files`
  * Add the template type as a suffix for template files
    * ie. `wildfly/files/idauth.override.properties.jinja`
  * Managed dot files should have dot_ as a prefix
    * ie. `wildfly/files/dot_bash_aliases` or `wildfly/files/dot_bashrc`
  * If the state needs a directory to make logical sense use the `init.sls` as an index of includes
    ```YAML
    include:
      - consul.prerequisites
      - consul.infrastructure
      - consul.services
    ```
  * The directory structure should be as follows:
    ```
    consul
      └── init.sls
      └── prerequisites.sls
        # this is other requirements before the application/state is installed (ie. java)
      └── infrastructure.sls
        # this includes user setup, directory and file creation states and installation
      └── files
        # This is where you store your jinja templates and other needed files for the installation
      └── any other needed states...
      └── services.sls
        # If it is a service you are running. Enable it in this state and run it last
    ```

## Readability

* Use *Break-out style* **not** *Cascading style*
  * This (*Break-out style*):
    ```YAML
    ensure_apache_is_running:
        service.running:
        - name: httpd
        - enabled: True
        # The package must be installed befor we can start the service
        - require: 
        - pkg: install_apache_package
    
    install_apache_package:
        pkg.installed:
        - name: httpd
    
    manage_httpd.conf_file:
        file.managed:
        - name: /etc/httpd/httpd.conf
        - source: salt://apache/files/httpd.conf.jinja
        - template: jinja
        # If this changes we want the service to be bounced
        - watch_in: 
            - service: ensure_apache_is_running
    ```

  * Instead of this (*Cascading style*):
    ```YAML
    httpd:
        service:
        - running:
        - enabled: True
        - require: 
            - pkg: httpd
        pkg:
        - installed
        file:
        - managed:
        - name: /etc/httpd/httpd.conf
        - source: salt://apache/files/httpd.conf.jinja
        - template: jinja
        - watch_in: 
            - service: ensure_apache_is_running
    ```

## Templating

* Use for information gathering
* Should only contain light conditionals and looping
* Use editor mode lines to avoid YAML mistakes
  * Example:  
    `# vim: syntax=yaml ts=2 sw=2 sts=2 et si ai`
* Seperate logic from states
  * This:
    ```YAML
    install_apache_package:
        pkg.installed:
        {% if grains.os_family == 'RedHat' %}
        - name: httpd
        {% elif grains.os_family == 'Debian' %}
        - name: apache2
        {% endif %}
    ```

  * Becomes this:
    ```YAML
    {% if grains.os_family == 'RedHat' %}
    {% set pkg_name = 'httpd' %}
    {% elif grains.os_family == 'Debian' %}
    {% set pkg_name = 'apache2' %}
    {% endif %}

    install_apache_package:
        pkg.installed:
        - name: {{ pkg_name }}
    ```

* Move comlpex queries into custome pillars/grains or maps
* Move complex control structures or logic into python
  * Custom modules
  * use python api

## Data Abstraction

* Use **include** statements to pull from other states
* Extend statement overrides an included state
  * Exmple:
    ```YAML
    include:
        - aux.td-agent

    extend:
        td-agent-config-file:
        file.managed:
            - name: /etc/td-agent/td-agent.conf
            - source: salt://aux/fluentd/td-agent.conf.jinja
            - template: jinja
            - context: 
                elastic_hosts: {{ salt['pillar.get']('apps:logs:elastic_hosts') }}
            - require:
            - pkg: td-agent
    ```

* Map files
  * Single location for reused values
  * Allows overrides
  * Solves many use-cases:
    * Platform specific details
    * Sane default values
    * Environment specific values
  * Example:
    ```yaml
    # /var/lib/salt/apache/map.sls
    {% set apache = salt.grains.filter_by({
        'Debian': {
            'pkg': 'apache2',
            'user': 'www-data',
            'config': '/etc/apache2/apache2.conf'
        },
        'RedHat': {
            'pkg': 'httpd',
            'user': 'apache'
            'config': '/etc/httpd/conf/httpd.conf'
        }
    }) %}
    ```

  * Beware of import syntax:
    * Oxygen (2018.3.0)
      ```yaml
      {% set apache = salt.slsutils.renderer('salt://apache/map.sls') %}
      ```
    * Pre-Oxygen (2017)
      ```yaml
      {% set apache = salt.slsutils.renderer(salt.cp.cahe_file('salt://apache/map.sls') %}
      ```
    * Old jinja (2015.5.3)
      ```yaml
      {% from 'apache/map.jinja' import apache with context %}
      ```

## Troubleshooting

* Know state system evaluation steps:
  * Templating engine evaluation (jinja)
  * Renderer evaluation (YAML)
  * High State
  * High Data
  * Low Data
  * Low Chunks
* Testing
  * Which layer are you testing?  
  `salt \* state.show_sls <state file>`  
  `salt \* state.apply <state file> test=True`  
  `salt \* state.apply <state file> mock=True`  
  `salt \* state.apply <state file>`  

## Tools

* Online YAML parser
  * https://yaml-online-parser.appspot.com
* jinjabread
  * https://jinjabread.com
* Kitchen-Salt
  * https://kitchen.saltstack.com
* Salt-Check
  * http://bit.ly/salt-check
